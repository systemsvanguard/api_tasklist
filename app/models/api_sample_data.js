/*
PostMan API client
------
POST	http://localhost:8080/api/tasks   
GET 	http://localhost:8080/api/tasks  

UPDATE 	http://localhost:8080/api/tasks/3 

Old: "description": "Task 3 ~ Description of task 08 08 to be changed" 
New: "description": "Task Three ~ Description of task 08 08 to be changed" 

*/

//  http://localhost:8080/api/tasks  

{
  "title": "One - New Recruit: Shipped, Not received (HH status 08 03)",
  "tasktype": "Automated",
  "weight": 700,
  "description": "Task 1 ~ Description of task 08 03",
  "published": false
}
{
  "title": "Two - New Recruit: Welcomed, Not installed (HH status 08 04)",
  "tasktype": "SharePoint",
  "weight": 650,
  "description": "Task 2 ~ Description of task 08 04",
  "published": true
}
{
  "id": 3,
  "title": "Three - New recruit: Installed, Not Welcomed (HH status 08 08)",
  "tasktype": "Automated",
  "weight": 750,
  "description": "Task 3 ~ Description of task 08 08 to be changed",
  "published": true
}
{
  "title": "Four - Home Due a 3 Month Thank You Call",
  "tasktype": "SharePoint",
  "weight": 170,
  "description": "Task 4 ~ Description of task 3 Month Demo Update",
  "published": false
}

{
  "title": "Five ~ Individual(s) in Vacation HH's with Minimum Motion (work order 458 & 459)",
  "tasktype": "Automated",
  "weight": 8000,
  "description": "Task 5 ~ Description of task 458 & 459",
  "published": false
}  


// Update | PUT  -->  http://localhost:8080/api/tasks/3  
{
    "description": "Task Three ~ Description of task 08 08 to be changed" 
}

// Find all Tasks where Published is True 
// GET --> http://localhost:8080/api/tasks?published=true 

// Find all Tasks containing the word 'Task'  
// GET --> http://localhost:8080/api/tasks?title=Welcomed 


/*
USE tasklistdb;
-- 
SELECT * FROM tasks; 
-- DROP TABLE tasks; 
*/

/*
INSERT INTO tasks (id, title, tasktype, weight, description, published, createdAt, updatedAt ) 
VALUES  
(7, 'Home Due a 6 Month Thank You Call', 'SharePoint', 160, 'Description of task 6 Month Demo Update.', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'),  
(8,	'HH with a Compliance Warning (contact code 888/999)', 'Automated', 100, 'Description of task 888 & 999 Warnings', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'),   
(9, 'Dropout HH returning or has returned (work order 914/915)',  'Automated', 0, 'Description of task 914 & 915', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'), 	
(10, '2 year old to recruit', 'Automated', 200, 'Description of task 950 - Baby Recruit', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'), 
(11, 'No Dock Individual (work order 486)', 'Automated', 140, 'Description of task 950 - G2 No Docks', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'), 
(12, 'AR call: Individual(s) with Excessive Motion', 'SharePoint', 350, 'Description of task 950 Analyst Excessive Motion', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'),  
(13, 'AR Call: Kids Docking after 11p/12p', 'SharePoint', 300, 'Description of task 950 Analyst Kids dock after 11p/Mdnt', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'),  
(14, 'AR Call: Possible Dual Carriage HH/Inds', 'SharePoint', 550, 'Description of task 950 Analyst request Dual Carriage', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58'), 
(15, 'AR Call: Excessive In/Out of Home Tuning', 'SharePoint', 450, 'Description of task 950 Analyst Request Excessive In Home/Excessive Out of Home', 0, '2021-09-07 19:27:58', '2021-09-07 19:27:58') 

*/
