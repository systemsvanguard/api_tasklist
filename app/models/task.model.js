module.exports = (sequelize, Sequelize) => {
  const Task = sequelize.define("task", {
    title: {
      type: Sequelize.STRING
    },
    tasktype: {
      type: Sequelize.STRING
    },  
    weight: {
      type: Sequelize.INTEGER
    }, 
    description: {
      type: Sequelize.STRING
    },   
    published: {
      type: Sequelize.BOOLEAN
    }
  });

  return Task;
};
