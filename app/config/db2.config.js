module.exports = {
  HOST: "localhost",
  USER: "numeris",
  PASSWORD: "MyP@55Word!",
  DB: "tasklistdb",
  dialect: "mysql",
  // dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
