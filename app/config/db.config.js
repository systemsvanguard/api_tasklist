/* 
RDDBS Connection Settings via Sequelize ORM 
===========================================
You must first install Sequelize ORM (via package.json)
Configuration settings below vary slightly dependent on the relational DB chosen. Configured now for MS SQL Server.
You will need to install the 'tedious' library/ driver to use MS SQL Server, OR the 'mysql2' library to use MySQL.
The SQL Server Browser service must be running on the MS SQL database server (check via 'Sql Server Configuration Manager'), AND UDP port 1444 on the database server must be reachable

In the RDBMS, you MUST first create the user, password, and database name! 
The database name is 'tasklistdb'. I have chosen to hide this and more importantly, the DB credentials, in an environment file ... 
... to prevent it being accidentally pushed to the Git or TFS code repository.
Please see included '.env4Display' file at project root for an example. 
You must first install 'dotenv' library (via package.json) to use environment files.

Explanation: 'max' & other times below are time in milli-seconds.

API reference for Sequelize ORM constructor : https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor   
Tedious library : https://www.npmjs.com/package/tedious 
MySQL library : https://www.npmjs.com/package/mysql2 
Sequelize ORM : https://www.npmjs.com/package/sequelize  
*/
module.exports = {
  // HOST: "DM-RHUNT-N1\MSSQLSERVER2", 
  // HOST: "localhost", 
  HOST: 'DM-RHUNT-N1', 
  PORT: '1433',  /* MS SQL Server database port */
  USER: 'tasklistuser',
  PASSWORD: 'MyP@55Word!', 
  DB: 'tasklistdb' ,
  // USER: process.env.USER,
  // PASSWORD: process.env.PASSWORD,
  // DB: process.env.DB ,
  dialect: "mssql", 
  // dialect: "mysql",     /* un-comment this line AND comment the sql server line above, to use MySQL DB instead */
  dialectOptions: {
    // encrypt: true, 
    instanceName: 'MSSQLSERVER2', 
    useUTC: false, 
    dateFirst: 1
  }, 
  pool: {
      max: 5, 
      min: 0, 
      acquire: 30000, 
      idle: 10000
  }
};
