/* 
RDDBS Connection Settings via Sequelize ORM 
===========================================
You must first install Sequelize ORM (via package.json)
Configuration settings below vary slightly dependent on the relational DB chosen. Configured now for MySQL.
You will need to install the 'mysql2' library to use MySQL.

In the RDBMS, you MUST first create the user, password, and database name! 
The database name is 'tasklistdb'. I have chosen to hide this and more importantly, the DB credentials, in an environment file ... 
... to prevent it being accidentally pushed to the Git or TFS code repository.
Please see included '.env4Display' file at project root for an example. 
You must first install 'dotenv' library (via package.json) to use environment files.

Explanation: 'max' & other times below are time in milli-seconds.

API reference for Sequelize ORM constructor : https://sequelize.org/master/class/lib/sequelize.js~Sequelize.html#instance-constructor-constructor   
MySQL library : https://www.npmjs.com/package/mysql2 
Sequelize ORM : https://www.npmjs.com/package/sequelize  
*/
module.exports = {
  // HOST: "localhost", 
  // USER: 'tasklistuser',
  // PASSWORD: 'aF@k3P@55Word!',  /* NOT a real password */
  // DB: 'tasklistdb' ,
  USER: process.env.USER,
  PASSWORD: process.env.PASSWORD,
  DB: process.env.DB ,
  dialect: "mysql",    
  pool: {
      max: 5, 
      min: 0, 
      acquire: 30000, 
      idle: 10000
  }
};
