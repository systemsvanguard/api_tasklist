// starting point for RESTful API
require('dotenv').config();
const express = require("express");
const cors = require("cors");
const path = require('path');
const port = process.env.PORT || 8080;
const appPort = process.env.PORT || 8080;
const apiPort = "http://localhost:8080/";
const exphbs = require('express-handlebars');

// create app
const app = express();

var corsOptions = {
  origin: apiPort 
};

app.use(cors(corsOptions));

// ------------------------------------
/* Use View Engine.  Must come before routing. */
app.engine('handlebars', exphbs() );
app.set('view engine', 'handlebars' );

// ------------------------------------
// Encoding!
// parse requests of content-type - application/json, AND requests of content-type - application/x-www-form-urlencoded
app.use(express.json()); 
app.use(express.urlencoded({ extended: true }));   

// ----- Database Connection -----------
const db = require("./app/models");
db.sequelize.sync();

/*
// drop the table if it already exists
db.sequelize.sync({ force: true }).then(() => {
   console.log("Drop and re-sync db.");
});
*/

// ----- Routing -----------------------
// app page routing
app.get('/', function(req, res) {
  res.render('home', {
    tagline:siteTagline,
    pageTitle : "Numeris Single Source Task List RESTful API prototype"
  });
} );

app.get('/about', function(req, res) {
  res.render('about');
} );

app.get('/contact', function(req, res) {
  res.render('contact');
} );

app.get('/services', function(req, res) {
  res.render('services');
} );

// simple API routing
require("./app/routes/task.routes")(app); 


// -------------------------------
// configure static files location
// app.use(express.static((__dirname + '/public' )) );
app.use(express.static(path.join((__dirname + '/public' ))) );

// universal variables
const siteTagline = "Shaping Canada's Media Landscape";


// ----- Start App -------------------------------
// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Numeris RESTful API web application now running on port ${PORT} at http://localhost:${PORT}/ . Enjoy!`);
});
